// This is the main.js file. Import global CSS and scripts here.
// The Client API can be used here. Learn more: gridsome.org/docs/client-api

import DefaultLayout from '~/layouts/Default.vue'
import DocsLayout from '~/layouts/Docs.vue'
import 'prismjs/themes/prism-tomorrow.css'
//  prism-coy.css        prism-solarizedlight.css
//  prism-dark.css       prism-tomorrow.css
//  prism-funky.css      prism-twilight.css
//  prism-okaidia.css    prism.css



export default function (Vue, { router, head, isClient }) {
  // Set default layout as a global component
  Vue.component('DocsLayout', DocsLayout)
  Vue.component('Layout', DefaultLayout)
}
