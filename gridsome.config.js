module.exports = {
  siteName: "ice",
  siteUrl: "https://alligator.io",
  siteDescription: "Learn about it on Alligator.io! 🐊",

  plugins: [
    {
      use: "@gridsome/vue-remark",
      options: {
        index: ["README"],
        baseDir: "./doc", // md files location
        pathPrefix: "/doc",
        typeName: 'DocPage',
        template: './src/templates/DocPage.vue',
        plugins: ["@gridsome/remark-prismjs"],
        remark: {
          autolinkHeadings: {
            content: {
              type: "text",
              value: "#"
            }
          }
        }
      }
    },
    {
      use: "gridsome-plugin-tailwindcss"
      /**
      options: {
        tailwindConfig: './tailwind.config.js',
        purgeConfig: {},
        presetEnvConfig: {},
        shouldPurge: true,
        shouldImport: true,
        shouldTimeTravel: true
      }
      */
    }
  ]
};
